class Suit {
    constructor() {
        // Property
        this.computerChoice = ["scissor", "paper", "rock"];

        this.scissorButton = document.getElementById("user-scissor");
        this.paperButton = document.getElementById("user-paper");
        this.rockButton = document.getElementById("user-rock");
        this.comScissor = document.getElementById("com-scissor");
        this.comPaper = document.getElementById("com-paper");
        this.comRock = document.getElementById("com-rock");

        this.result = document.getElementById("result");     
    }
    // Method
    randomChoice() {
        return this.computerChoice[Math.floor(Math.random() * this.computerChoice.length)];
    }
    play(userChoice) {
        this.disable();
        const computerChoice = this.randomChoice();
        console.log(`Player 1 chose ${userChoice.toUpperCase()} and COM chose ${computerChoice.toUpperCase()}`);
        this.retryButton();
        let state = userChoice;
        if(userChoice === computerChoice) {
            this.setResult("draw");
            switch(computerChoice) {
                case "scissor":
                    this.comScissor.classList.add("activeChoice");
                    break;
                case "rock":
                    this.comRock.classList.add("activeChoice");
                    break;
                case "paper":
                    this.comPaper.classList.add("activeChoice");
                    break;
            }
        }
        else {
            switch(state) {
                case "scissor":
                    if(computerChoice === "paper") {
                        this.comPaper.classList.add("activeChoice");
                        this.setResult("win");
                    } 
                    else if (computerChoice === "rock") {
                        this.comRock.classList.add("activeChoice");
                        this.setResult("lose");
                    }
                    break;
                case "paper":
                    if(computerChoice === "scissor") {
                        this.comScissor.classList.add("activeChoice");
                        this.setResult("lose");
                    }
                    else if(computerChoice === "rock") {
                        this.comRock.classList.add("activeChoice");
                        this.setResult("win");
                    }
                    break;
                case "rock":
                    if(computerChoice === "scissor") {
                        this.comScissor.classList.add("activeChoice");
                        this.setResult("win");
                    }
                    else if(computerChoice === "paper") {
                        this.comPaper.classList.add("activeChoice");
                        this.setResult("lose");
                    }
                    break;
            }
        }
    }
    setResult(outcome) {
        switch(outcome) {
            case "win":
                document.getElementById("result").id = "winResult";
                document.getElementById("winResult").innerHTML = "PLAYER 1<br/> WIN";
                console.log("Result : Player 1 WIN");
                break;
            case "lose":
                document.getElementById("result").id = "loseResult";
                document.getElementById("loseResult").innerHTML = "COM<br/> WIN";
                console.log("Result : COM WIN");
                break;
            case "draw":
                document.getElementById("result").id = "drawResult";
                document.getElementById("drawResult").innerHTML = "DRAW";
                console.log("Result: Draw");
                break;
        }
    }
    disable() {
        let disableButton = document.querySelectorAll("button");
        disableButton.forEach(function(button) {
            button.classList.add("disabled");
            button.disabled = true;
        });
    }
    
    start() {
        this.paperButton.addEventListener('click', () => {
            this.play("paper");
            this.paperButton.classList.add("activeChoice");
        });
        this.rockButton.addEventListener('click', () => {
            this.play("rock");
            this.rockButton.classList.add("activeChoice");
        });
        this.scissorButton.addEventListener('click', () => {
            this.play("scissor");
            this.scissorButton.classList.add("activeChoice");
        });
    }
    retryButton() {
        var retry = document.getElementById("retry-button");
        let disabledButton = document.querySelectorAll("button");
        let result = document.querySelector("h2"); 
        retry.classList.remove("d-none");
        retry.addEventListener('click', function() {
            result.innerHTML = "VS";
            //console.log("retry button was clicked");
            retry.classList.add("d-none");
            result.id = "result";
            disabledButton.forEach(function(b) {
                b.classList.remove("disabled");
                b.disabled = false;
            })
            let activeChoice = document.querySelectorAll(".activeChoice");
            for(let i = 0; i < activeChoice.length; i++) {
                activeChoice[i].classList.remove("activeChoice");
            }  
        });
    }
}


const game = new Suit();
game.start();