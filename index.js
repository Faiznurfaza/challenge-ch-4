var i = 0;
var txt = "Welcome to My Website\nLets play Rock Paper Scissor Game";
var speed = 70;

function typeWriter() {
    if (i < txt.length) {
        document.getElementById("animation-text").innerHTML += txt.charAt(i);
        i++;
        setTimeout(typeWriter, speed);
    } else {
        setTimeout(function(){
            let button = document.querySelector("a");
            button.classList.remove("d-none");}, 500);
    }
}

typeWriter();
